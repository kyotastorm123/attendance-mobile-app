//
//  ViewController.swift
//  Attendance
//
//  Created by Juan Pablo Gonzalez P on 8/10/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate, CBPeripheralManagerDelegate {
    
    @IBOutlet weak var ErrorMessage: UILabel!
    
    @IBOutlet weak var Registered: UILabel!
    
    @IBOutlet weak var Loading: UIActivityIndicatorView!
    
    var centralManager:CBCentralManager!
    var peripheralManager:CBPeripheralManager = CBPeripheralManager()
    let uuid:CBUUID = CBUUID(string: "DCEF54A2-31EB-467F-AF8E-350FB641C97B")
    var hidden = true
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
        print("started advertising")
        print(peripheral)
    }


    private func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        print("peripheral discovered")
        print("peripheral: \(peripheral)")
        print("advertisement: \(advertisementData)")
        if let data = advertisementData["kCBAdvDataServiceData"] {
            print("found advert data: \(data)")
        }
        print("RSSI: \(RSSI)")
    }

    func startAdvert(){
        let advertisingData = [CBAdvertisementDataLocalNameKey:"my-peripheral", CBAdvertisementDataServiceUUIDsKey: uuid] as [String : Any]
        peripheralManager.startAdvertising(advertisingData)
    }

    private func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("peripheral disconnected")
        print("peripheral: \(peripheral)")
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("central state updated")
        print(central.description)
        if central.state == .poweredOff {
            print("bluetooth is off")
        }
        if central.state == .poweredOn {
            print("bluetooth is on")
            centralManager.scanForPeripherals(withServices: nil, options: [ CBCentralManagerScanOptionAllowDuplicatesKey : true])
            startAdvert()
        }
        if central.state == .unsupported {
            print("bluetooth is unsupported on this device")
        }
    }

    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print("peripheral state updated")
        print("\(peripheral.description)")
    }
    
    
    
    @IBAction func ActivateBluetooth(_ sender: UIButton) {
        
        
        Loading.isHidden = false;
        Loading.setNeedsDisplay()
        Loading.startAnimating()

         self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
         self.centralManager = CBCentralManager(delegate: self, queue: nil)
         let advertisingData = [CBAdvertisementDataLocalNameKey:"my-peripheral", CBAdvertisementDataServiceUUIDsKey: uuid] as [String : Any]
         print("advertising data test - \(advertisingData)")
         peripheralManager.startAdvertising(advertisingData)
         centralManager.scanForPeripherals(withServices: [uuid], options: [ CBCentralManagerScanOptionAllowDuplicatesKey : true])

        
        
         httpRequest();

         centralManager.stopScan()
    
        
        Loading.isHidden = true;
        
        
        Registered.isHidden = !hidden
        ErrorMessage.isHidden = hidden;

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Registered.isHidden = true;
        ErrorMessage.isHidden = true;
        Loading.isHidden = true;
    }
    
    
    private func httpRequest() {
        
        let date = Date()
        let formatter = DateFormatter()
        
        var equal = false
        
        formatter.dateFormat = "dd-MM-yyyy"
        
        let result = formatter.string(from: date)

        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/\(result)/students/")!
        //let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/14-7-2019/students/")!
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        let request = URLRequest(url: url)
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    if let array = json["documents"] as? [[String: Any]] {
                        for object in array {
                            // access all objects in array
                            if let fields = object["fields"] as? [String: Any] {
                                // access individual value in dictionary
                                if let mac = fields["mac"] as? [String: Any] {
                                    // access individual value in dictionary
                                    if "\(mac)" == "[\"stringValue\": DC:08:0F:10:E5:45]" {
                                        equal = true
                                        print("Equal\(equal)")
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        hidden = equal
        task.resume()
        
    }
    
    
    /*
    private func studentList() {
        
        //create the url with NSURL
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList/201424703")!
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        let request = URLRequest(url: url)
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    print(json)
                    if let fields = json["fields"] as? [String: Any] {
                        // access individual value in dictionary
                        if let mac = fields["mac"] as? [String: Any] {
                            // access individual value in dictionary
                            print("entro?\(mac)")
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    */
}




